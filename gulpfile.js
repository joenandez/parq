var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var minifyCss = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var webserver = require('gulp-webserver');

gulp.task('sass', function () {
    gulp.src('style.scss')
        .pipe(sass())
        .pipe(gulp.dest('css'));
});

gulp.task('css', function () {
    return gulp.src('css/style.css')
        .pipe(autoprefixer({
            browsers: ['last 10 versions'],
            cascade: true
        }))
        .pipe(minifyCss())
        .pipe(gulp.dest('css'));
});

gulp.task('compress', function() {
  return gulp.src('app.js')
    .pipe(uglify())
    .pipe(gulp.dest('js'));
});

gulp.task('images', function () {
    return gulp.src('img/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('img'));
});

gulp.task('up', function(){
	gulp.src('')
		.pipe(webserver({
      		open: true
    	}));
	gulp.watch('style.scss', ['sass']);
});

gulp.task('build', ['css', 'compress', 'images']);
